/**
 * \file MCP4161.cpp
 * This file implements a "driver" for the Microchip MCP4161 digital potentiometer, for use with \b Arduino (and other
 * compatible) devices.
 *
 * \brief MCP4161 Arduino library (driver) implementation
 *
 * \version 0.1.0
 *
 * \author		Gerad Munsch <gmunsch@unforgivendevelopment.com>
 * \date		2017
 * \copyright	BSD license
 */


#include "MCP4161.h"
#include "Arduino.h"
#include <SPI.h>



MCP4161::MCP4161(unsigned char csPin) {
	_pin_cs(csPin);
}


void MCP4161::begin() {
	/* Setup pin(s) */
	pinMode(this->_pin_cs, OUTPUT);

	/* Ensure chip is not selected right away */
	spiChipDisable();

	/* Start SPI, and set configuration */
	SPI.begin();
	SPI.setBitOrder(MSBFIRST);
	SPI.setDataMode(DEFAULT_SPI_MODE);
	SPI.setClockDivider(DEFAULT_SPI_SPEED);
}


void MCP4161::spiChipEnable() {
	/* take the SS pin low to select the chip: */
	digitalWrite(this->_pin_cs, LOW);
}


void MCP4161::spiChipDisable() {
	/* take the SS pin high to de-select the chip: */
	digitalWrite(this->_pin_cs, HIGH);
}


uint8_t MCP4161::spiWriteByte(uint8_t dataByte, bool firstWrite, bool finalWrite) {

	if (firstWrite == true) {
		spiChipEnable();
	}

	uint8_t ret = SPI.transfer(dataByte);

	if (finalWrite == true) {
		spiChipDisable();
	}

	return ret;
}



uint16_t spiWriteWord(uint16_t dataWord, bool firstWrite = true, bool finalWrite = true) {

	if (firstWrite == true) {
		spiChipEnable();
	}

	uint16_t ret = SPI.transfer16(dataWord);

	if (finalWrite == true) {
		spiChipDisable();
	}

	return ret;
}


uint8_t MCP4161::increment() {
	return spiWriteByte(MCP4161_INCREMENT_CURRENT_VALUE);
}


int MCP4161::decrement() {
	enable();

	/* send in the address and value via SPI: */
	byte ret1 = SPI.transfer(MCP4161_DECREMENT_CURRENT_VALUE);

	disable();

	return ret1;
}


int MCP4161::readTCON() {
	enable();

	/* send in the address and value via SPI: */
	byte ret1 = SPI.transfer(0x4C);
	byte ret2 = SPI.transfer(0x00);

	disable();

	return ret1;
}


int MCP4161::initTCON() {
	enable();

	/* send in the address and value via SPI: */
	byte ret1 = SPI.transfer(0x41);
	byte ret2 = SPI.transfer(0x0F);

	disable();

	return ret1;
}


int MCP4161::readStatus() {
	enable();

	/* send in the address and value via SPI: */
	byte ret1 = SPI.transfer(0x5C);
	byte ret2 = SPI.transfer(0x00);

	disable();

	return ret1;
}


int MCP4161::setTap(int value) {
	enable();

	/* send in the address and value via SPI: */
	byte hiByte = 0x03 & (value >> 8);
	byte loByte = 0x00FF & value;

#ifdef ENABLE_DEBUG_PRINTS
	Serial.print("HIGH: ");
	Serial.println(hiByte, BIN);
	Serial.print("LOW: ");
	Serial.println(loByte, BIN);
#endif

	byte ret1 = SPI.transfer(hiByte);
	byte ret2 = SPI.transfer(loByte);

	disable();

	return (ret1 << 8) | ret2;
}
