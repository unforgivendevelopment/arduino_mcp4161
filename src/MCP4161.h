/*
 * MCP4161.h
 * Part of the 'Arduino_MCP4161' library for Arduino
 *
 * (c) 2017 Gerad Munsch <gmunsch@unforgivendevelopment.com>
 *
 */


#ifndef _MCP4161_H__
#define _MCP4161_H__


#include "Arduino.h"
#include <SPI.h>


/**
 * Enable debugging output to the serial console by uncommenting the line below.
 * Likewise, disable these prints by commenting the line below.
 *
 * The funtionality is handled by the defintion of 'ENABLE_DEBUG_PRINTS'
 */
#define ENABLE_DEBUG_PRINTS 1

/**
 * Define the pin numbers of the SPI interface.
 *
 * \todo This definition seems to be limited to a specific subset of Arduino-compatible devices. This will need to be
 *       reworked to either use the platform definitions, define various platforms here using #ifdef statements (or a
 *       similar mechanism), or ensure that they are able to be defined in the user sketch by some means.
 */
#define MOSI	11
#define MISO	12
#define SCK		13

/**
 * Defines other aspects of the SPI interface.
 */
#define DEFAULT_SPI_SPEED_READ	SPI_CLOCK_DIV128	/*!< We can only read from the device at ~125KHz */
#define DEFAULT_SPI_SPEED_WRITE	SPI_CLOCK_DIV4		/*!< We can write to the device at 4 MHz */
#define DEFAULT_SPI_MODE		SPI_MODE0			/*!< Use SPI mode 0 */

/**
 * Define the range of values accepted by the MCP4161
 */
#define MCP4161_MIN_VALUE	0x0000
#define MCP4161_MID_VALUE	0x0080
#define MCP4161_MAX_VALUE	0x0100

/**
 * Define MCP4161 instruction/command values
 */
#define MCP4161_INCREMENT_CURRENT_VALUE	0x04
#define MCP4161_DECREMENT_CURRENT_VALUE	0x08




class MCP4161 {
public:
	MCP4161(unsigned char csPin = 10);

	int initTCON();
	int readTCON();
	int readStatus();
	uint8_t increment();
	int decrement();
	int setTap(int value);

private:
	unsigned char _pin_cs;

	void spiChipEnable();
	void spiChipDisable();

	/**
	 * Write a single byte (8 bits) to the SPI bus, with the option to keep a device selected
	 * while awaiting additional data.
	 *
	 * @brief Write a single byte to the SPI bus, optionally as part of a continued transaction
	 *
	 * @param[in] dataByte   The byte to be written to the SPI bus.
	 * @param[in] firstWrite (OPTIONAL) Defines whether this is the first write in a transaction (selects chip). Defaults to 'true'.
	 * @param[in] finalWrite (OPTIONAL) Defines whether this is the final write in a transaction (deselects chip). Defaults to 'true'.
	 *
	 * @return One byte (8 bits) of data read from the SPI bus as a result of the transaction.
	 *
	 */
	uint8_t spiWriteByte(uint8_t dataByte, bool firstWrite = true, bool finalWrite = true);

	/**
	 * Write a single word (16 bits) to the SPI bus, optionally keeping the device selected, awaiting additional data.
	 *
	 * \brief Write a single word to the SPI bus, optionally as part of a continued transaction
	 *
	 * \param[in]	dataWord	The word to be written to the SPI bus.
	 * \param[in]	firstWrite	(OPTIONAL) Defines whether this is the first write in a transaction (selects chip).
	 *							Defaults to 'true'.
	 * \param[in]	finalWrite	(OPTIONAL) Defines whether this is the final write in a transaction (deselects chip).
	 * 							Defaults to 'true'.
	 *
	 * \return One word (16 bits) read from the SPI bus as a result of the transaction.
	 */
	uint16_t spiWriteWord(uint16_t dataWord, bool firstWrite = true, bool finalWrite = true);

};

#endif	/* __MC_SRC_MCP4161_H__P4161_H_ */
