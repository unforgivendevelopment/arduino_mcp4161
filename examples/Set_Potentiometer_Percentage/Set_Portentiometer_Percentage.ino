/**
 * \file Set_Portentiometer_Percentage.ino
 * An example to set the digital potentiometer as a percentage value, rather than a bit value.
 *
 * \author		Gerad Munsch <gmunsch@unforgivendevelopment.com>
 * \date		2017
 *
 */

#include <SPI.h>
#include <MCP4161.h>
