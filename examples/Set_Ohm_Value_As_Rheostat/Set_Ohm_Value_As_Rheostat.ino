/**
 * \file Set_Ohm_Value_As_Rheostat.ino
 * An example to use the digital potentiometer as a rheostat, setting the desired resistance in ohms. For this to work,
 * the specific "total ohms" value of the MCP4161 in use must be defined.
 *
 * \author		Gerad Munsch <gmunsch@unforgivendevelopment.com>
 * \date		2017
 *
 */

#include <SPI.h>
#include <MCP4161.h>
