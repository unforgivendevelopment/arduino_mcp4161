/**
 * \file Sweep_Potentiometer_Range.ino
 * An example to sweep the entire range of the digital potentiometer.
 *
 * \author		Gerad Munsch <gmunsch@unforgivendevelopment.com>
 * \date		2017
 *
 */

#include <SPI.h>
#include <MCP4161.h>
