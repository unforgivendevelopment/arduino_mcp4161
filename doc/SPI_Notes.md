# Arduino SPI Notes #

As the **[MCP4161][1]** uses the _**SPI** interface_, which is a highly flexible interface, and capabilities of the interface, and the configuration of such capabilities varies from device to device, as many of those details as possible will be detailed in this file.


## SPI Clock Speed ##


### Arduino Uno ###

At the default clock rate of the _Arduino Uno_, **16 MHz**, the following list details the clock divisors, and the resulting SPI clock rates:
* ``SPI_CLOCK_DIV2``	- 8 MHz
* ``SPI_CLOCK_DIV4``	- 4 MHz
* ``SPI_CLOCK_DIV8``	- 2 MHz
* ``SPI_CLOCK_DIV16``	- 1 MHz
* ``SPI_CLOCK_DIV32``	- 500 KHz
* ``SPI_CLOCK_DIV64``	- 250 KHz
* ``SPI_CLOCK_DIV128``	- 125 KHz


## SPI Modes ##

The **[MCP4161][1]** supports the following **SPI** modes:
* ``SPI_MODE0`` - _This is a **relatively safe** default to use for most **SPI** devices._
* ``SPI_MODE3`` - _While not quite as universally supported as ``SPI_MODE0``, many devices support both ``SPI_MODE3`` as well as ``SPI_MODE0``.

The use of ``SPI_MODE1`` and ``SPI_MODE2`` are quite uncommon, and are unsupported by the **[MCP4161][1]**, as well as many other devices.


## SPI Data Ordering ##

The **[MCP4161][1]** requires that data is sent in ``MSBFIRST`` (ie: **most-significant bit (MSB) first**) format.







[1]:	<https://microchip.com/FIXME/MCP4161>
