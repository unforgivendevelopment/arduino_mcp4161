# Arduino Library for Microchip MCP4161 #

This Arduino library provides the ability to interface with the [Microchip][1a] [**MCP4161**][1b] digital potentiometer.


## Hardware Information ##

The [**MCP4161**][1b] digital potentiometer is a non-volatile, 8-bit _(257 steps)_, utilizing **EEPROM** for the non-volatile storage, and **SPI** for the host interface.


### **SPI Interface** -- _Unique Challenges_ ###

Due to the fact that this device is an 8-pin device, the **SPI** interface is not able to provide all 4 pins.
As such, the **MOSI** and **MISO** signals are multiplexed onto a single pin.

Communcation with the device is predominantly in the direction of:

    [MASTER/UC] --> [MCP4161]

However, there are some commands which need to send data back to the master _(microcontroller)_.









[1a]:	<https://www.microchip.com>
[1b]:	<https://www.microchip.com/wwwproducts/en/MCP4161>
